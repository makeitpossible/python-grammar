# 扩展的两个内容：
# 需求：把元组中的每个数据按照位置参数的方式进行传参
# 需求：把字典中的每项数据按照关键字参数的方式进行传参


def show(*args, **kwargs):
    print("args:", args, type(args))
    print("kwargs:", kwargs, type(kwargs))


# 元组
my_tuple = (1, 3)
# 列表
my_list = [1, 3]
# 字典
my_dict = {"a": 1, "b": 2}

# 此时这里的传参方式还是按照位置参数的方式进行传参
show(my_tuple, my_dict)  # 不是我想要的传参方式
show(my_tuple[0], my_tuple[1], a=my_dict["a"], b=my_dict["b"])

# 扩展：
# 把元组中的每个数据按照位置参数的方式进行传参， *my_tuple
# 把字典中的每项数据按照关键字参数的方式进行传参，**my_dict
show(*my_tuple, **my_dict)  # => show(1, 3, a=1, b=2)

# 注意点：
# *my_tuple, **my_dict 不能单独使用，只能结合函数或者方法使用
