# +=的本质就是在原有的空间地址基础上修改数据，修改后内存地址不变，提示：想要在原有内存空间的基础上修改数据
# 前提是该数据就是一个可变类型的数据
# python中所有的赋值都是引用的传递

def modify_value(params):
    # print("params:", params, id(params))
    print("修改前params:", params, id(params))  # 修改前params: 10 140712304563520
    # 修改后，内存空间地址发生变化
    params += params
    print("修改后params:", params, id(params))  # 修改后params: 20 140712304563840


# 定义不可变类型的数据，
my_num = 10
print("my_num:", my_num, id(my_num))

# 在python里面所有的赋值操作都是按照引用传递的(按照使用的内存地址来传递的)
modify_value(my_num)  # params = my_num
