# 在python里面定义类分两种；
# 1.旧式类（python2)
# 2.新式类(python3)

# 旧式类语法格式
# class 类名:
#     属性和方法

# 新式类语法格式
# class 类名(object):
#     属性和方法

# 类名要使用大驼峰命名法，类名和变量名的组成一样，都是由字母、数字、下划线组成，不能以数字开头
# 定义类的关键字是class

# object类时所有类的最顶级类，也就是咱们的根类
class Teacher(object):
    # 属性和方法
    # 方法
    def show(self):
        print("哈哈，我是一个方法！")


# 函数在类里面称为方法，其实方法的本质就是函数。




