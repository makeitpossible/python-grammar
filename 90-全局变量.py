# 全局变量：在函数外定义的变量称为全局变量

# 全局变量的作用域(使用范围): 可以在不同函数之间使用或者函数外使用，也可以理解成在当前的整个python文件中可以使用

# 全局变量的作用：可以在不同函数之间共享全局变量的数据

# 定义全局变量
g_score = 100


def show_msg():
    print("show_msg内访问当前全局变量的数据为：", g_score)


def show_data():
    print("show_data内访问当前全局变量的数据为：", g_score)


show_msg()

print("在函数外访问全局变量的数据为：", g_score)
