# 提示：object类时所有类的最顶级类，也就是根类
class Teacher(object):
    # 属性和方法
    def teach(self):
        print("我有授课的能力")


# 函数在类里面称为方法，其实方法的本质就是函数
# 创建对象的语法格式
# 变量 = 类名()

# 创建对象
t1 = Teacher()

# 给对象添加属性
t1.name = "李四"
t1.age = 30
t1.sex = "男"

# 获取对象的属性
print(t1.name)
print(t1.age)
print(t1.sex)

# 注意点：必须给对象添加好了属性，然后才能获取对象的属性。
# 再创建对象
t2 = Teacher()
# 给创建好的对象添加属性
t2.name = "王五"
t2.age = 31
t2.sex = "男"

# 获取对象属性
print(t2.name, t2.age, ta2.sex)


