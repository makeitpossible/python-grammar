# 不定长参数：在定义函数的时候，参数的个数不确定，该参数称为不定长参数，该参数称为不定长参数

# 不定长产生细分为两种
# 1. *args 表示不定长位置参数，专门来接收不确定个数的位置参数的
# 2. **kwargs 表示不定长关键字参数，专门来接收不确定格式的关键字参数的


# 带有不定长位置参数的函数
# 提示：args参数名可以修改，但是一般 不改，约定俗成就要args
def show_info(*args):
    print("args:", args, type(args))  # args: ('李四', 20, '男') <class 'tuple'>


# 调用函数，使用位置参数的方式进行传参
# 提示：调用函数的时候会把所有的位置参数打包成一个元组，然后把元组给到args参数，则args的类型是元组
show_info("李四", 20, "男")  # 调用函数的时候，这里的参数使用位置参数

show_info()  # 传0个参数，返回的是一个空的元组


# 带有不定长关键字参数的函数我=========================
def show_datat(**kwargs):
    print("kwargs:", kwargs, type(kwargs))
    # kwargs: {'a': 1, 'b': 2} <class 'dict'>


# 调用函数，使用关键字参数方式进行传参
# 提示：调用函数的时候会把所有的关键字参数打包成一个字典，然后把字典给kwargs参数
# 调用函数，使用关键字参数的方式进行传参
show_datat(a=1, b=2)
show_datat()


# 带有不定长位置参数和不定长关键字参数的函数 =============
def show(*args, **kwargs):
    print("args:", args, type(args))
    # args: (1, 3, 5) <class 'tuple'>
    print("kwargs:", kwargs, type(kwargs))
    # kwargs: {'a': 2, 'b': 4} <class 'dict'>


show(1, 3, 5, a=2, b=4)
