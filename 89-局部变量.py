# 局部变量：在函数内定义的变量称为局部变量

# 局部变量的作用域（使用范围）：只能在当前函数内使用，不能在其他函数使用

# 局部变量的作用：临时存储函数的数据，当函数执行结束的时候，局部变量就会在内存中销毁

# 达成共识的知识点：程序运行后，程序中的数据加载到内存里面


def show():
    # 定义的是一个局部变量
    name = "吕布"
    print("三国第一勇将:", name)


show()
# 错误演示：在函数外或其他函数内无法使用=====
# print(name)
"""NameError: name 'name' is not defined"""


def show_msg():
    print(name)


show_msg()
"""NameError: name 'name' is not defined"""

