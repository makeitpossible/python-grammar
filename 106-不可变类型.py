# 不可变类型：不允许在原有内存空间的基础上修改数据，修改后变量保存的内存地址会发生变化。想要修改必须要重新赋值。

# 不可变类型有：数字类型，字符串，元组，range

my_str = "abc"

print("修改前：", my_str, id(my_str))
# 字符串不允许根据下标在原有内存空间的基础上修改数据
# my_str[i] = "d"

# 可以给变量重复赋值一个修改后的数据
my_str = "adc"
print("修改后：", my_str, id(my_str))

print("=============================")

my_num = 10

print("修改前：", my_num, id(my_num))
# my_num[0] = 2

# 修改数据是通过重新赋值来完成的
my_num = 20
print("修改后：", my_num, id(my_num))

# my_range = range(10)

# my_range[0] = 1

my_tuple = (1, 3, 10)

# my_tuple[1] = 20
my_tuple = (1, 20, 10)

print(my_tuple)
