# 拆包：使用不同变量保存容器类型中的每一个数据，该操作称为拆包
# 提示：拆包只能对容器类型的数据进程拆包
# 容器类型：字符串，range, 列表，元组，字典，集合等等

my_str = "ab"

# 利用拆包获取容器类型中的每个数据
str1, str2 = my_str
print(str1, str2)  # a b

# 注意点：变量的个数一定要和容器类型中数据个数对应
v1, v2 = range(1, 5, 2)  # 1, 3
print(v1, v2)  # 1 3

my_list = ["A", "a"]
data1, data2 = my_list
print(data1, data2)  # A a

# 提示：对字典进行拆包，默认得到的是key
person_dict = {"name": "李四", "age": 20}
key1, key2 = person_dict

# print(key1, key2)

value1, value2 = person_dict.values()
print(value1, value2)  # 李四 20

# 拆包应用的场景
# 函数的返回值是多个数据，可以怼函数的返回值进行拆包


def return_more_value():
    return 1, 3, 5


# 对函数的返回值进行拆包
num1, num2, num3 = return_more_value()
print(num1, num2, num3)  # 1 3 5


