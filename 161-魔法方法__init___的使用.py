# 魔法方法： 方法名的前后有两个下划线(__),该方法称为魔法方法，比如：__init__就是一个魔法方法
# 魔法方法的特点：当完成某种操作的时候就会自动调用魔法方法，比如：当对象创建完成以后会自动调用__init__方法

# __init__方法表示是对象的初始化方法，作用就是给创建好的对象添加属性

# 提示：以后给对象添加属性的时候的操作统一要在魔法方法__init__里面完成


# 定义学生类
class Student(object):

    # 提示：提供__init__方法目的就是给创建好的对象添加属性
    def __init__(self):
        # 给对象添加属性
        stu1.no = 1
        stu1.name = "李四"
        stu1.age = 25

    # 显示对象的属性信息
    def show(self):  # 提示：self参数名可以进行修改，但是一般不会进行修改，约定俗成写成self
        # pass
        # 由于代码写死，只能获取stu1这个对象的属性信息
        print(stu1.no, stu1.name, stu1.age)

        # self 表示调用当前方法时的对象
        print("self:", id(self))



# 总结：通过以上代码的演示, __init__方法确实可以给创建好的对象添加属性，只不过属性值都写死了
# 解决方法：__init__方法带有参数，可以给不同的属性设置不同的属性信息。

