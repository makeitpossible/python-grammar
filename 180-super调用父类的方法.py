class Person(object):

    def __init__(self, name, age):
        # 父类里面提高的公共属性
        self.name = name
        self.age = age

    def sleep(self):
        print("人正在睡觉！")


class Student(Person):

    def __init__(self, no):
        # Person.__init__(name,age)
        self.no = no

    def run(self):

        # 1. self调用父类的方法，注意点：self能够调用父类的方法，前提子类里面不能由父类同名的方法
        # self.sleep()  # 提示：对象调用对象方法不需要自己动手传参
        # 2. 父类的类名调用父类的方法，注意点：父类的类名调用父类的方法容易受到类名的影响
        # Person.sleep(self)  # 提示：类调用对象方法需要自己的self参数进行传参
        # 需求：在run方法类调用父类的方法
        print("学生正在跑步")
        # super：本质上是一个类，super()表示创建一个父类的对象可以调用父类的方法，比如super().sleep()
        super().sleep()
        print("学生正在跑步")


stu = Student(1)
stu.run()
