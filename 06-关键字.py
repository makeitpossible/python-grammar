# 导入关键字模块，模块通俗理解就是一个python文件
import keyword  # ctr + b 转到代码的定义

# 关键字：具有特殊功能的标识符为关键字，比如：break,continue,if

# 关键字的注意点：关键字不能作为变量名使用

# 查看关键字
result = keyword.kwlist
print(result)
# 返回的是列表
print(type(result))
# 查看关键字的个数35个
print(len(result))

# 错误的测试，关键字不能作为变量名使用
# False = 'abc'
# break = 1

# int不是关键字，可以作为变量名使用，但是不推荐使用，因为有一个类名叫int
# ['False', 'None', 'True', 'and', 'as', 'assert', 'async', 'await', 'break', 'class',
# 'continue', 'def','del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global',
# 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return',
# 'try', 'while', 'with', 'yield']
# <class 'list'>
