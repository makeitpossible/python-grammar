# 调用函数的三种传参方式
# 1. 按照位置参数的方式传参，注意点：位置参数的顺序一定要和函数定义时参数的顺序保持一致。
# 2. 按照关键字参数的方式进行传参，注意点：关键字的名字要和函数定义时参数的名字保持一致。
# 3. 前面使用位置参数后面使用关键字参数的方式进行传参
    # 注意点：
    # 一旦前面使用了关键字参数进行传参，则后面必须使用关键字参数进行传参，不能再使用位置参数的形式传参


def show_info(name, age, sex):
    print("姓名：", name, "年龄：", age, "性别：", sex)


# ========================= 按照位置参数的方式进行传参 =====================
# show_info("李四", 20, "男")
# 注意点：位置参数的顺序一定要和函数定义时参数的顺序保持一致
# show_info("王五", "男", "30")

# ========================= 按照关键字参数的方式进行传参 ====================
show_info(name="李四", age=20, sex="男")
# 错误的传参方式：注意点：关键字的名字要和函数定义时参数的名字保持一致。
show_info(name="李四", age=20, sex1="男")
# 按照关键字的方式进行传参，不强调关键字的顺序
show_info(age=20, name="李四", sex="男")

# =======================前面使用位置参数，后面使用关键字参数方式传参============
show_info("张飞", age=30, sex="男")

# 一旦前面使用了关键字参数进行传参，则后面必须使用关键字参数进行传参，不能再使用位置参数的形式传参
show_info("张飞", age=30, "男")




