# 函数的文档说明：多行注释放到了函数内部的第一个行我们称之为函数的文档说明
# 函数文档说明的作用：就是对函数的功能进行解释说明


def sum_num():
    """实现两个数字的求和操作"""
    num1 = 1
    num2 = 3

    result = num1 + num2
    print("结果为：", result)


# 查看函数的问题说明，使用help函数
help(sum_num)
