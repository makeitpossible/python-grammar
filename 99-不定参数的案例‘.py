# 需求： 定义一个函数，完成对不确定个数的参数进行求和操作

# 定义带有不定长参数的函数
def sum_num(*args, **kwargs):
    print("args:", args, type(args))
    print("kwargs:", kwargs, type(kwargs))
    # 保存所有参数和的结果
    result = 0
    # 如何把所有的参数全部加起来
    for value in args:
        # print("位置参数有：", value)
        result += value
    print("===================")

    for value in kwargs.values():
        print("关键字参数有：", value)
        result += value

    # 把计算后的结果进行返回
    return result


sum_num(1, 2)
sum_num(a=3, b=4)

data = sum_num(1, 3, a=2, b=4)
print("结果为：", data)  # 结果为： 10

data = sum_num(1, 3)
print("结果为：", data)  # 结果为： 4

data = sum_num()
print("结果为：", data)  # 结果为： 0
