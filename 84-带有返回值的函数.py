# 带有返回值的函数：函数调用完成以后会给调用者一个数据（给调用者的这个数据成为函数的返回值）

# 带有返回值的函数的语法格式：
# def 函数名(形参1, 形参2, ...):
#
#     return 数据

# 函数带有返回值的表现形式：函数内部使用return数据


# 定义带有返回值的函数
def sum_num(n1, n2):
    result = n1 + n2
    # 通过return 把result这个变量返回值返回给函数的调用者
    return result


# 调用函数,使用value变量保存函数调用后返回的结果
value = sum_num(1, 2)
print("获取到的返回值为：", value)
# 如果没有return将值返回，value保存的数据为空：None
