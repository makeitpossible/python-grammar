# 错误的演示 =================================
def return_more_value():
    print("==========1==========")
    return 1
    print("==========2==========")
    return 2


# 使用变量result保存调用函数后返回的数据
result = return_more_value()
print("返回的数据为:", result)

# 总结：
# 通过以上代码的演示，可以得知
# 1. 当函数执行完return关键字以后，表示函数执行结束，return语句后面的代码不会执行。
# 2. 通过return只能返回1次数据
print("============================================")


# 解决方案：return可以返回一个容器类型的数据，这样可以包含多个数据。
def return_more_data():
    # 返回列表
    # return [1, 3, 5]
    # 返回字典
    # return {"name": "李四", "age": 20}
    # 返回元组的简写方式
    return 1, 4, 6


result = return_more_data()
print(result, type(result))

my_tuple = 1, 0, 3
print(my_tuple, type(my_tuple))
"""(1, 0, 3) <class 'tuple'>"""
