# 多个函数之间共享数据的操作
# 1.全局变量
# 2.函数变的返回值
import random

# =========================全局变量共享数据的操作=================
g_num = 10


def task1():
    # 声明修改全局变量的数据
    global g_num
    g_num = 9


def task2():
    print("全局变量的数据为:", g_num)


# 调用函数
task1()
task2()

# ========================函数的防止在不同函数值共享数据的操作================


def return_value():
    name_list = ["西施", "貂蝉", "王昭君", "杨玉环"]
    # 想要把列表中的某一个数据共享给其他函数使用
    index = random.randint(0, len(name_list) - 1)
    name = name_list[index]
    return name


def show_name():
    # 调用return_value的函数，通过返回key把数据共享给其他函数
    name = return_value()
    print("获取的美女为:", name)


def show_data():
    # 调用return_value的函数，通过返回key把数据共享给其他函数
    name = return_value()
    print("美女为:", name)


show_name()
show_data()




