num1 = 3
num2 = 5

# 需求：交换两个变量的值

# 交换两个变量值的通用写法 ====
temp = num1
num1 = num2
num2 = temp

print(num1, num2)  # 5 3

# 在python里面交换两个变量值的简写方式
num1, num2 = num2, num1
print(num1, num2)  # 3 5

# 对上面的代码进行解析
# 1. 先把num2和num1的数据组包到元组
result = num2, num1
print(result, type(result))  # (5, 3) <class 'tuple'>
# 2. 利用拆包交换两个变量的值
num1, num2 = result
print(num1, num2)  # 5 3



