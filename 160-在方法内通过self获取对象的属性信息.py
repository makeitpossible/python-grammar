# 定义学生类
class Student(object):

    # 显示对象的属性信息,这里的self可以改成任意的参数，但不建议修改，约定俗成为self
    def show(self):
        # pass  # 就是占位符，空的实现
        # 由于代码写死，我们只能获取第一个对象的属性信息
        # print(stu1.name, stu1.no, stu1.age)
        # self表示调用当前方法的对象
        print("self:", id(self))  # self: 1890509296192

        # 提示：以后可以用self来获取不同对象的属性信息
        print(self.no, self.name, self.age)


# 创建对象
stu1 = Student()
print("stu1:", id(stu1))  # stu1: 1890509296192
# 给对象添加属性
stu1.no = 1
stu1.name = "李四"
stu1.age = 20

# print(stu1.name, stu1.no, stu1.age)  # 李四 1 20
# 使用对象调用show方法，显示对象的属性信息
stu1.show()  # => 隐含操作---stu1.show(stu1),self=stu1,python解释器会把stu1传给self






