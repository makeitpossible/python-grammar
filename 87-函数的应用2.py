# 需求：写一个函数求三个函数的和


def sum_num(num1, num2, num3):
    # 实现求三个数字的和
    result = num1 + num2 + num3
    # 返回计算后的结果
    return result


# value = sum_num(1, 2, 3)
# print("结果为:", value)


# 需求：写一个函数求三个数的平均值
def avg_value(n1, n2, n3):
    # 1. 先求三个数字的和
    # result = n1 + n2 + n3
    result = sum_num(n1, n2, n3)
    # 2. 除以3求出平均值
    return result / 3  # 返回计算后的平均值


# 使用current_value变量保存函数调用后返回的结果
current_value = avg_value(1, 2, 3)
print("平均值为：", current_value)
