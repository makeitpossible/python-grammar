# 利用函数执行return语句后，函数就会结束的特点，把多层函数终止执行


def show_data():
    for outer in range(1, 4):
        print("外层循环的数据为：", outer)

        for inner in range(2, 10, 2):
            print("内存循环的数据为：", inner)
            if inner == 6:
                # 需求：让所有的循环终止
                return   # 当函数执行完成return关键字函数表示执行结束，循环就不会再执行了


show_data()

# 总结：
# 当函数执行return表示函数执行结束，可以让多层循环终止执行。

